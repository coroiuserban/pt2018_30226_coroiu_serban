package model;

import java.util.LinkedList;
import java.util.Queue;

public class Coada {
	private Queue<Client> queue;
	private int totalTime;
	
	
	public Coada() {
		queue  = new LinkedList<Client>();
		
	}
	
	public int getTotalTime() {
		return totalTime;
	}
	
	public void increaseQueueTime(int time) {
		totalTime = totalTime + time;
	}
	
	public void add(Client client) {
		queue.add(client);
		totalTime += client.getServiceTime();
	}
	
	public boolean isEmpty() {
		return queue.isEmpty();
	}
	
	public Client remove() {
		return queue.remove();
	}
	public Client peek() {
		return queue.peek();
	}
	
	public int size() {
		return queue.size();
	}
	
	public Queue<Client> getQueue() {
		return queue;
	}
	
	
}
