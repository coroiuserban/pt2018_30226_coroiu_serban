package model;
	
public class Client {
	private String nume;
	private int arrivalTime;
	private int serviceTime;
	private int finishTime;
	private String coada;
	
	public Client(String nume) {
		this.nume = nume;
	}
	
	public Client(String nume, int serviceTime) {
		this.nume = nume;
		this.serviceTime = serviceTime;
	}
	
	public String getNume() {
		return nume;
	}
	
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	
	public void setFinishTime(int finishTime) {
		this.finishTime = finishTime;
	}
	
	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}

	public int getServiceTime() {
		return serviceTime;
	}
	
	public int getArrivalTime() {
		return arrivalTime;
	}
	
	public int getFinishTime() {
		return finishTime;
	}
	
	public void setCoada(String coada) {
		this.coada = coada;
	}
	
	public String toString() {
		return nume + " se afla la: " + coada+   " arrival time: " + arrivalTime + " finish time: " + finishTime ;
	}
}
