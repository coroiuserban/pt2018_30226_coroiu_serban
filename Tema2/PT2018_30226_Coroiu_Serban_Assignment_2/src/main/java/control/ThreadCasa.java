package control;


import java.util.LinkedList;
import java.util.Queue;

import model.Client;
import model.Coada;
import view.Gui;

public class ThreadCasa extends Thread implements Runnable{
	Coada coadaCasa = new Coada();
	Client currQueueClient = null;
	private Queue<Client> currClients = new LinkedList<Client>();
	int currClientTime;
	Gui gui = new Gui();
	private int time;
	private int simTime, waitingTime, clientsNo, serviceTime, maxClients, maxTime;
	int totalService;
	public ThreadCasa(String name) {
		super(name);
		waitingTime = 0;
		clientsNo = 0;
		simTime = 0;
		currClientTime = 0;
		time = 0;
		serviceTime = 0;
		maxClients = 0;
	}
	public ThreadCasa() {
		
	}
	public void run() {
		while(true && time != simTime) {
			if(currClientTime == 0 && !coadaCasa.isEmpty()) {
				currQueueClient = coadaCasa.peek();
				currClientTime = currQueueClient.getServiceTime();
				currQueueClient.setCoada(getName());
			}
			if(currClients.size() >= maxClients) {
				maxClients = currClients.size();
				maxTime = time;				//momentul in care avem cei mai multi clienti
			}
			if( currClientTime != 0) {
				//System.out.println(this.getName() +" are "+ clientsNo +" clienti si timp de asteptare "+ waitingTime);
				time++;
				currClientTime-- ;
				totalService--;
				if(currClientTime == 0) {
					currQueueClient.setFinishTime(time);
					coadaCasa.remove();   
					//System.out.println(currQueueClient.getNume()+" iese la "+ this.getName());
					//Gui.append(currQueueClient.toString());
					Gui.append(currQueueClient.getNume() + " iese de la " + this.getName() + " la " + time);
					//System.out.println(currQueueClient.getNume() + " iese de la " + this.getName() + " la " + time);
					waitingTime += currQueueClient.getFinishTime() - currQueueClient.getArrivalTime();
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public void addClientQueue(Client client) {
		client.setArrivalTime(time);
		coadaCasa.add(client);
		totalService += client.getServiceTime();
		serviceTime += client.getServiceTime();
		clientsNo++;
		Gui.append(client.getNume() + " a ajuns la " + this.getName() + " la " + time);
		//System.out.println(client.getNume() + " a ajuns la " + this.getName() + " la " + time);
		//System.out.println(client.getNume()+" intra la "+ this.getName());
	}
	public int getTotalTime() {
		return totalService;
	}
	
	public Queue<Client> getCoada() {
		return coadaCasa.getQueue();
	}
	
	public void setSimTime(int simTime) {
		this.simTime = simTime;
	}
	
	public double getAvgWaitingTime() {
		if(clientsNo != 0)
			return ((double)waitingTime)/clientsNo;
		return 0.0;
	}
	
	public double getAvgServiceTime() {
		System.out.println(this.getName() + " are serv " + serviceTime + " " + clientsNo);
		if(clientsNo != 0)
			return ((double)serviceTime)/clientsNo;
		return 0.0;
	}
	
	public int getPeakTime() {
		return maxTime;
	}
}
