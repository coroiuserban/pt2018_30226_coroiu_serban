package control;

import java.util.LinkedList;
import java.util.Queue;

import model.Client;
import view.Gui;

public class Control {
	public static void adaugaClientiRandom(int arrMin, int arrMax, int serMin, int serMax, int queueNo, int simTime, int clientsNo) {
		Gui gui = new Gui();
		Queue<Client> coadaMagazin = new LinkedList<Client>();
		
		int serviceTime;
		String nume;
		for(int i=0;i<clientsNo;i++) {
			nume = "Client " + i;
			serviceTime = (int)(Math.random()*(serMax - serMin + 1) + serMin);
			Client client = new Client(nume, serviceTime);
			coadaMagazin.add(client);
			gui.clients.append(client.getNume() +" are timpul de servire: "+ client.getServiceTime()+"\n");
		}
		ThreadGeneral th = new ThreadGeneral(queueNo, simTime, arrMin, arrMax);
		th.setCoada(coadaMagazin);
		th.start();
	}
}
