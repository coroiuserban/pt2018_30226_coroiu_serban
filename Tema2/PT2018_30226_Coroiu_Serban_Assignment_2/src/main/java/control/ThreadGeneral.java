package control;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import model.Client;
import view.Gui;

public class ThreadGeneral extends Thread implements Runnable{
	int time=0;
	boolean ok = true;
	private int queueNo;
	private int simTime, arrMin, arrMax;
	static ArrayList<ThreadCasa> casa = new ArrayList<ThreadCasa>();
	private Queue<Client> coadaMagazin = new LinkedList<Client>();
	
	public ThreadGeneral(int queueNo, int simTime, int arrMin, int arrMax) {
		this.queueNo = queueNo;
		this.simTime = simTime;
		this.arrMin = arrMin;
		this.arrMax = arrMax;
	}
	public ThreadGeneral() {
		//fa instanta la obiecte aici 
	}
	
	@SuppressWarnings("deprecation")
	public void run() {
		for(int i=0;i<queueNo;i++) {
			casa.add(new ThreadCasa("Casa "+i));
			casa.get(i).setSimTime(simTime);
			casa.get(i).start();
		}
		while(ok) {
			try {
				int c = (int) (Math.random()*(arrMax - arrMin + 1) + arrMin);
				Thread.sleep(1000*c);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(!coadaMagazin.isEmpty()) {
				int minQueue = findMin();
				System.out.println(minQueue);
				Gui.append(coadaMagazin.peek().getNume() + " merge la Casa " + minQueue);
				casa.get(minQueue).addClientQueue(coadaMagazin.remove());
			}else {
				ok=false;
				for(ThreadCasa t : casa) {
					if(t.isAlive())
						ok = true;
					if(t.getTotalTime() == 0) {
						t.stop();
					}
				}
			}
			time++;
		}
		this.stop();
	}
	public int findMin() {
		int minTime = 99;
		int pos=0;
		int i=0;
		for(ThreadCasa th : casa) {
			if(th.getTotalTime() < minTime) {
				minTime = th.getTotalTime();
				pos = i;
			}
			i++;
		}
		return pos;
	}
	
	public void setCoada(Queue<Client> coadaMagazin) {
		this.coadaMagazin = coadaMagazin;
	}
	
	public Queue<Client> getCoadaCasa(int i) {
		return casa.get(i).getCoada();
	}
	
	public double getWait(int i) {
		return casa.get(i).getAvgWaitingTime();
	}
	public double getService(int i) {
		return casa.get(i).getAvgServiceTime();
	}
	public int getPeak(int i) {
		return casa.get(i).getPeakTime();
	}
	public boolean getStatus() {
		return ok;
	}
}
