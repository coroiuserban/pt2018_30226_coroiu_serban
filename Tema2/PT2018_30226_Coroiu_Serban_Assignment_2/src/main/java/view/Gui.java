package view;



import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Queue;
import javax.swing.*;


import control.Control;

import control.ThreadGeneral;
import model.Client;

public class Gui extends Thread implements Runnable{
	public Gui() {
		startGui();
	}
	public JFrame frame;
	
	private int arrMin, arrMax;
	private int serMin, serMax;
	private int queueNo;
	private static int simTime;
	private int clientsNo;
	private boolean status = false;
	
	private void startGui() {
		frame = new JFrame("QueueSimulator");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		frame.setResizable(false);
		frame.setBounds(400, 100, 360, 400);
		
		JPanel panel1 = new JPanel();
		panel1.setBounds(10, 10, 470, 450);
		panel1.setLayout(null);
		frame.add(panel1);
		
		JLabel arrTime = new JLabel("Introduceti timpul minim si maxim de sosire intre clienti:");
		arrTime.setBounds(10, 20, 350, 20);
		panel1.add(arrTime);
		
		JLabel lMin = new JLabel("min: ");
		lMin.setBounds(50, 50, 40, 20);
		panel1.add(lMin);
		
		final JTextField tMin = new JTextField();
		tMin.setBounds(90, 50, 30, 20);
		panel1.add(tMin);
		
		JLabel lMax = new JLabel("max: ");
		lMax.setBounds(190, 50, 40, 20);
		panel1.add(lMax);
		
		final JTextField tMax = new JTextField();
		tMax.setBounds(230, 50, 30, 20);
		panel1.add(tMax);
		
		JLabel serTime = new JLabel("Introduceti timpul minim si maxim de servire:");
		serTime.setBounds(10, 80, 350, 20);
		panel1.add(serTime);
		
		JLabel slMin = new JLabel("min: ");
		slMin.setBounds(50, 100, 40, 20);
		panel1.add(slMin);
		
		final JTextField stmin = new JTextField();
		stmin.setBounds(90, 100, 30, 20);
		panel1.add(stmin);
		
		JLabel slMax = new JLabel("max: ");
		slMax.setBounds(190, 100, 40, 20);
		panel1.add(slMax);
		
		final JTextField stMax = new JTextField();
		stMax.setBounds(230, 100, 30, 20);
		panel1.add(stMax);
		
		JLabel noQueue = new JLabel("Introduceti numarul de cozi: ");
		noQueue.setBounds(10, 130, 180, 20);
		panel1.add(noQueue);
		
		final JTextField tnoQueue = new JTextField();
		tnoQueue.setBounds(180, 130, 30, 20);
		panel1.add(tnoQueue);

		JLabel lSimTime = new JLabel("Introduceti timpul de simulare: ");
		lSimTime.setBounds(10, 180, 180, 20);
		panel1.add(lSimTime);
		
		final JTextField tSimTime = new JTextField();
		tSimTime.setBounds(190, 180, 30, 20);
		panel1.add(tSimTime);
		
		JLabel lNoClients = new JLabel("Introduceti numarul de clienti: ");
		lNoClients.setBounds(10, 230, 180, 20);
		panel1.add(lNoClients);
		
		final JTextField tNoClients = new JTextField();
		tNoClients.setBounds(190, 230, 30, 20);
		tNoClients.setText("9");
		panel1.add(tNoClients);
 
		JButton btnSubmit = new JButton("Start simulation!");
		btnSubmit.setBounds(80, 270, 150, 20);
		panel1.add(btnSubmit);
		
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					arrMin = Integer.parseInt(tMin.getText());
					arrMax = Integer.parseInt(tMax.getText());
					serMin = Integer.parseInt(stmin.getText());
					serMax = Integer.parseInt(stMax.getText());
					queueNo = Integer.parseInt(tnoQueue.getText());
					simTime = Integer.parseInt(tSimTime.getText());
					clientsNo = Integer.parseInt(tNoClients.getText());
				}catch(Exception e1) {
					System.out.println("Parse error\n");
				}
				status = true;
				Control.adaugaClientiRandom(arrMin, arrMax, serMin, serMax, queueNo, simTime, clientsNo);
				createResultWindow();				
			}
		});
	}
	
	public int getArrMin() {
		return arrMin;
	}
	
	public int getArrMax() {
		return arrMax;
	}
	
	public int getSerMax() {
		return serMax;
	}
	
	public int getSerMin() {
		return serMin;
	}
	
	public int getQueueNo() {
		return queueNo;
	}
	
	public int getSimTime() {
		return simTime;
	}
	
	public int getClientsNo() {
		System.out.println(clientsNo);
		return clientsNo;
	}
	
	public static final JTextArea rez = new JTextArea();
	public static final JTextArea clients = new JTextArea();
	private static final ArrayList<JTextArea> queueClients = new ArrayList<JTextArea>();
	private JScrollPane scroll = new JScrollPane();
	private JScrollPane scrollClients = new JScrollPane();
	public JFrame frame1 = new JFrame();
	public JPanel j = new JPanel();
	private JTextArea coada = new JTextArea();
	private JLabel timeLabel = new JLabel();
	
	public void createResultWindow() {
		frame1 = new JFrame("Simulation Result");
		frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame1.setLayout(null);
		frame1.setVisible(true);
		frame1.setBounds(400, 100, 510, 800);
		//JPanel mainPanel = new JPanel();
		j = new JPanel();
		j.setLayout(null);
		j.setBounds(10, 10,	480, 800);

		//timeLabel.setBounds(255, 0, 30, 20);
		//j.add(timeLabel);
		
		scrollClients.setBounds(10, 20, 450, 220);
		scrollClients.setViewportView(clients);
		j.add(scrollClients);
		
		scroll.setBounds(10, 250, 450, 220);
		scroll.setViewportView(rez);
		j.add(scroll);
		

		for(int i=0;i<queueNo;i++) {
			coada = new JTextArea();
			coada.setBounds(10, i*40+480, 450, 30);
			queueClients.add(coada);
			j.add(queueClients.get(i));
		}
		frame1.add(j);
		
		frame.setVisible(false);
	}

	public void run() {
		boolean ok = true;
		int time = 0 ;
		ThreadGeneral casa = new ThreadGeneral();
		while(ok) {
			if(status == true) {
				timeLabel.setText(String.valueOf(time++ + 1));
				int i = 0;
				for(JTextArea textArea : queueClients) {
					textArea.setText("");
					Queue<Client> queue = casa.getCoadaCasa(i++);
					for(Client client : queue) {
						textArea.append(client.getNume()+ " ");
					}
				}
			}
			try {
				if(time == simTime && time != 0)
					ok = false;
					//this.stop();
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		generateReport();
	}
	public void generateReport() {
		JFrame frame = new  JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		System.out.println(queueNo);
		GridLayout grid = new GridLayout(0, queueNo + 1);
		frame.setLayout(grid);
		frame.setBounds(100, 500, 600, 400);
		
		JLabel freeLabel = new JLabel("");
		frame.add(freeLabel);
		
		
		for(int i=0;i<queueNo;i++) {
			JLabel labelNo = new JLabel("Casa " + i);
			frame.add(labelNo);
		}
		
		JLabel waitLabel = new JLabel("Average waiting time");
		frame.add(waitLabel);
		ThreadGeneral th = new ThreadGeneral();
		for(int i=0;i<queueNo;i++) {
			double waitTime = th.getWait(i);
			//System.out.println(waitTime);
			JLabel labelwait = new JLabel(String.valueOf(waitTime));
			frame.add(labelwait);
		}
		
		JLabel serLabel = new JLabel("Average service time");
		frame.add(serLabel);
		
		for(int i=0;i<queueNo;i++) {
			double waitTime = th.getService(i);
			//System.out.println(waitTime);
			JLabel labelwait = new JLabel(String.valueOf(waitTime));
			frame.add(labelwait);
		}
		
		JLabel peekLabel = new JLabel("Average peak time");
		frame.add(peekLabel);
		
		for(int i=0;i<queueNo;i++) {
			int peakTime = th.getPeak(i);
			//System.out.println(waitTime);
			JLabel labelpeak = new JLabel(String.valueOf(peakTime));
			frame.add(labelpeak);
		}
		
		frame.setVisible(true);

	}
	public static void append(final String string) {
		rez.append(string + "\n");
	}
}
