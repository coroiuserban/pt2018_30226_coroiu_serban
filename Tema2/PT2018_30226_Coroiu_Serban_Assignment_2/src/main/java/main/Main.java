package main;

import view.Gui;

public class Main {
	public static void main(String argv[]) {
	
		Gui gui = new Gui();
		gui.frame.setVisible(true);
		gui.start();
	}
		
}
